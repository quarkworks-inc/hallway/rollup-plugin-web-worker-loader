/**
 *
 * @param {string} url File name, e.g. 'web-worker-0.js'
 * @param {string?} basePath config.loadPath or config.outputFolder
 * @returns
 */
export function createURLWorkerFactory(url, basePath) {
    /**
     * @param {WorkerOptions} workerOptions
     * @param {{ url?: string, useObjectUrl?: boolean }} factoryOptions
     */
    return function WorkerFactory(workerOptions, factoryOptions) {
        // Optional defaults
        factoryOptions = Object.assign(
            {
                url: undefined,
                useObjectUrl: false,
            },
            factoryOptions
        );

        let workerUrl = factoryOptions.url || new URL(url, basePath);

        if (factoryOptions.useObjectUrl) {
            // Workaround for browser security restrictions
            const workerSrc = `importScripts("${workerUrl}")`;
            workerUrl = URL.createObjectURL(new Blob([workerSrc]), {
                type: "text/javascript",
            });
        }
        return new Worker(workerUrl, workerOptions);
    };
}
